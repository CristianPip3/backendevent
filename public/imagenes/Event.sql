/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     04/03/2014 11:27:12 a.m.                     */
/*==============================================================*/


alter table ACTOR
   drop constraint FK_ACTOR_REFERENCE_REPRESEN;

alter table ACTOR
   drop constraint FK_ACTOR_REFERENCE_PAIS;

alter table ACTUA
   drop constraint FK_ACTUA_REFERENCE_PELICULA;

alter table ACTUA
   drop constraint FK_ACTUA_REFERENCE_ACTOR;

alter table PELICULA
   drop constraint FK_PELICULA_REFERENCE_CATEGORI;

drop table ACTOR cascade constraints;

drop table ACTUA cascade constraints;

drop table CATEGORIA cascade constraints;

drop table PAIS cascade constraints;

drop table PELICULA cascade constraints;

drop table REPRESENTANTE cascade constraints;

/*==============================================================*/
/* Table: ACTOR                                                 */
/*==============================================================*/
create table ACTOR 
(
   ACT_ID               INTEGER              not null,
   REP_ID               INTEGER,
   PAI_ID               INTEGER              not null,
   ACT_NOMBRE           VARCHAR(40)          not null,
   ACT_TELEFONO         VARCHAR(30),
   ACT_FECHANACIMIENTO  DATE,
   constraint PK_ACTOR primary key (ACT_ID)
);

/*==============================================================*/
/* Table: ACTUA                                                 */
/*==============================================================*/
create table ACTUA 
(
   PEL_ID               INTEGER              not null,
   ACT_ID               INTEGER              not null,
   SALARIO              NUMBER(8,0)          not null,
   NUM_CONTRATO         INTEGER              not null,
   constraint PK_ACTUA primary key (PEL_ID, ACT_ID)
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA 
(
   CAT_ID               INTEGER              not null,
   CAT_NOMBRE           VARCHAR(40)          not null,
   CAT_DESCRIPCION      VARCHAR(500),
   constraint PK_CATEGORIA primary key (CAT_ID)
);

/*==============================================================*/
/* Table: PAIS                                                  */
/*==============================================================*/
create table PAIS 
(
   PAI_ID               INTEGER              not null,
   PAI_NOMBRE           VARCHAR(30)          not null,
   constraint PK_PAIS primary key (PAI_ID)
);

/*==============================================================*/
/* Table: PELICULA                                              */
/*==============================================================*/
create table PELICULA 
(
   PEL_ID               INTEGER              not null,
   CAT_ID               INTEGER              not null,
   PEL_NOMBRE           VARCHAR(40)          not null,
   PEL_RESUMEN          VARCHAR(500),
   PEL_CLASIFICACION    VARCHAR(25)         
      constraint CKC_PEL_CLASIFICACION_PELICULA check (PEL_CLASIFICACION is null or (PEL_CLASIFICACION in ('Todos','mayor6anios','mayor14anios'))),
   PEL_FECHA_ESTRENO    DATE                 not null,
   constraint PK_PELICULA primary key (PEL_ID)
);

/*==============================================================*/
/* Table: REPRESENTANTE                                         */
/*==============================================================*/
create table REPRESENTANTE 
(
   REP_ID               INTEGER              not null,
   REP_NOMBRE           VARCHAR(40)          not null,
   REP_MAIL             VARCHAR(40),
   constraint PK_REPRESENTANTE primary key (REP_ID)
);

alter table ACTOR
   add constraint FK_ACTOR_REFERENCE_REPRESEN foreign key (REP_ID)
      references REPRESENTANTE (REP_ID);

alter table ACTOR
   add constraint FK_ACTOR_REFERENCE_PAIS foreign key (PAI_ID)
      references PAIS (PAI_ID);

alter table ACTUA
   add constraint FK_ACTUA_REFERENCE_PELICULA foreign key (PEL_ID)
      references PELICULA (PEL_ID);

alter table ACTUA
   add constraint FK_ACTUA_REFERENCE_ACTOR foreign key (ACT_ID)
      references ACTOR (ACT_ID);

alter table PELICULA
   add constraint FK_PELICULA_REFERENCE_CATEGORI foreign key (CAT_ID)
      references CATEGORIA (CAT_ID);



insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (1, 'Acci�n', 'Peliculas de acci�n');

insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (2, 'Drama', 'Peliculas de drama');

insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (3, 'Terror', 'Peliculas de terror');

insert into REPRESENTANTE (REP_ID, REP_NOMBRE, REP_MAIL) values (1, 'Jose Martin', NULL);

insert into REPRESENTANTE (REP_ID, REP_NOMBRE, REP_MAIL) values (2, 'Sara Tomson', 'saratom@representantes.com');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (1, 'Estados Unidos');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (2, 'Brasil');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (3, 'Mexico');

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN, PEL_FECHA_ESTRENO) values (1, 2, 'Her', 'En un futuro cercano, Theodore, un hombre solitario se enamora de la voz de su sistema operativo', TO_DATE('21/02/2014', 'dd/mm/yyyy'));

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN, PEL_FECHA_ESTRENO) values (2, 1, '300 : El origen de un imperio', 'El general griego Temistocles lucha por conseguir la unidad de las polis griegas.', TO_DATE('13/04/2014', 'dd/mm/yyyy'));

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN, PEL_FECHA_ESTRENO) values (3, 3, 'Yo, Frankenstein', 'Adaptaci�n del c�mic de Dark Storm pero ambientada en el presente', TO_DATE('10/02/2014', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (1, 1 , 1, 'Joaquin Phoenix', NULL, TO_DATE('28/10/1974', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (2, 2 , 1, 'Scarlett Johansson', NULL, TO_DATE('22/11/1984', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (3, NULL , 2, 'Rodrigo Santoro', NULL, TO_DATE('22/08/1975', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (4, NULL , 3, 'Salma Hayek', NULL, TO_DATE('02/09/1966', 'dd/mm/yyyy'));


insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (1, 1, 2000000, 120);

insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (1, 2, 3000000, 184);

insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (2, 3, 1500000, 109);
////////////////////////////////////////////////////////////////////////////////
/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     18/03/2014 11:08:43 a.m.                     */
/*==============================================================*/


alter table ACTOR
   drop constraint FK_ACTOR_REFERENCE_REPRESEN;

alter table ACTOR
   drop constraint FK_ACTOR_REFERENCE_PAIS;

alter table ACTUA
   drop constraint FK_ACTUA_REFERENCE_PELICULA;

alter table ACTUA
   drop constraint FK_ACTUA_REFERENCE_ACTOR;

alter table BITACORA_ACTOR_PELICULA
   drop constraint FK_BITACORA_REFERENCE_ACTOR;

alter table PELICULA
   drop constraint FK_PELICULA_REFERENCE_CATEGORI;

drop table ACTOR cascade constraints;

drop table ACTUA cascade constraints;

drop table BITACORA_ACTOR_PELICULA cascade constraints;

drop table CATEGORIA cascade constraints;

drop table PAIS cascade constraints;

drop table PELICULA cascade constraints;

drop table REPRESENTANTE cascade constraints;

drop table REPRESENTANTE_ELIMINADO cascade constraints;

/*==============================================================*/
/* Table: ACTOR                                                 */
/*==============================================================*/
create table ACTOR 
(
   ACT_ID               INTEGER              not null,
   REP_ID               INTEGER,
   PAI_ID               INTEGER,
   ACT_NOMBRE           VARCHAR(40),
   ACT_TELEFONO         VARCHAR(30),
   ACT_FECHANACIMIENTO  DATE,
   constraint PK_ACTOR primary key (ACT_ID)
);

/*==============================================================*/
/* Table: ACTUA                                                 */
/*==============================================================*/
create table ACTUA 
(
   PEL_ID               INTEGER              not null,
   ACT_ID               INTEGER              not null,
   NUM_CONTRATO         INTEGER,
   SALARIO              FLOAT,
   constraint PK_ACTUA primary key (PEL_ID, ACT_ID)
);

/*==============================================================*/
/* Table: BITACORA_ACTOR_PELICULA                               */
/*==============================================================*/
create table BITACORA_ACTOR_PELICULA 
(
   ACT_ID               INTEGER              not null,
   HA_FECHA_CAMBIO      DATE                 not null,
   HA_USUARIO           VARCHAR2(50)         not null,
   HA_NOMBRE_NUEVO      VARCHAR2(40),
   HA_TELEFONO_NUEVO    VARCHAR2(30),
   HA_FECHA_NAC_NUEVA   DATE,
   constraint PK_BITACORA_ACTOR_PELICULA primary key (ACT_ID, HA_FECHA_CAMBIO, HA_USUARIO)
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA 
(
   CAT_ID               INTEGER              not null,
   CAT_NOMBRE           VARCHAR(40),
   CAT_DESCRIPCION      VARCHAR(500),
   constraint PK_CATEGORIA primary key (CAT_ID)
);

/*==============================================================*/
/* Table: PAIS                                                  */
/*==============================================================*/
create table PAIS 
(
   PAI_ID               INTEGER              not null,
   PAI_NOMBRE           VARCHAR(30),
   constraint PK_PAIS primary key (PAI_ID)
);

/*==============================================================*/
/* Table: PELICULA                                              */
/*==============================================================*/
create table PELICULA 
(
   PEL_ID               INTEGER              not null,
   CAT_ID               INTEGER,
   PEL_NOMBRE           VARCHAR(40),
   PEL_RESUMEN          VARCHAR(500),
   PEL_CLASIFICACION    VARCHAR(25)         
      constraint CKC_PEL_CLASIFICACION_PELICULA check (PEL_CLASIFICACION is null or (PEL_CLASIFICACION in ('Todos','mayor6anios','mayor14anios'))),
   PEL_PRESUPUESTO      FLOAT,
   constraint PK_PELICULA primary key (PEL_ID)
);

/*==============================================================*/
/* Table: REPRESENTANTE                                         */
/*==============================================================*/
create table REPRESENTANTE 
(
   REP_ID               INTEGER              not null,
   REP_NOMBRE           VARCHAR(40),
   REP_MAIL             VARCHAR(40),
   constraint PK_REPRESENTANTE primary key (REP_ID)
);

/*==============================================================*/
/* Table: REPRESENTANTE_ELIMINADO                               */
/*==============================================================*/
create table REPRESENTANTE_ELIMINADO 
(
   RE_ID                INTEGER              not null,
   RE_NOMBRE            VARCHAR(40),
   RE_MAIL              VARCHAR(40),
   RE_FECHA_ELIMINACION DATE,
   RE_USURIO            VARCHAR2(50),
   constraint PK_REPRESENTANTE_ELIMINADO primary key (RE_ID)
);

alter table ACTOR
   add constraint FK_ACTOR_REFERENCE_REPRESEN foreign key (REP_ID)
      references REPRESENTANTE (REP_ID);

alter table ACTOR
   add constraint FK_ACTOR_REFERENCE_PAIS foreign key (PAI_ID)
      references PAIS (PAI_ID);

alter table ACTUA
   add constraint FK_ACTUA_REFERENCE_PELICULA foreign key (PEL_ID)
      references PELICULA (PEL_ID);

alter table ACTUA
   add constraint FK_ACTUA_REFERENCE_ACTOR foreign key (ACT_ID)
      references ACTOR (ACT_ID);

alter table BITACORA_ACTOR_PELICULA
   add constraint FK_BITACORA_REFERENCE_ACTOR foreign key (ACT_ID)
      references ACTOR (ACT_ID);

alter table PELICULA
   add constraint FK_PELICULA_REFERENCE_CATEGORI foreign key (CAT_ID)
      references CATEGORIA (CAT_ID);



insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (1, 'Acci�n', 'Peliculas de acci�n');

insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (2, 'Drama', 'Peliculas de drama');

insert into CATEGORIA (CAT_ID, CAT_NOMBRE, CAT_DESCRIPCION) values (3, 'Terror', 'Peliculas de terror');

insert into REPRESENTANTE (REP_ID, REP_NOMBRE, REP_MAIL) values (1, 'Jose Martin', NULL);

insert into REPRESENTANTE (REP_ID, REP_NOMBRE, REP_MAIL) values (2, 'Sara Tomson', 'saratom@representantes.com');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (1, 'Estados Unidos');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (2, 'Brasil');

insert into PAIS (PAI_ID, PAI_NOMBRE) values (3, 'Mexico');

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN) values (1, 2, 'Her', 'En un futuro cercano, Theodore, un hombre solitario se enamora de la voz de su sistema operativo');

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN) values (2, 1, '300 : El origen de un imperio', 'El general griego Temistocles lucha por conseguir la unidad de las polis griegas.');

insert into PELICULA (PEL_ID, CAT_ID, PEL_NOMBRE, PEL_RESUMEN) values (3, 3, 'Yo, Frankenstein', 'Adaptaci�n del c�mic de Dark Storm pero ambientada en el presente');

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (1, 1 , 1, 'Joaquin Phoenix', NULL, TO_DATE('28/10/1974', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (2, 2 , 1, 'Scarlett Johansson', NULL, TO_DATE('22/11/1984', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (3, NULL , 2, 'Rodrigo Santoro', NULL, TO_DATE('22/08/1975', 'dd/mm/yyyy'));

insert into ACTOR (ACT_ID, REP_ID, PAI_ID, ACT_NOMBRE, ACT_TELEFONO, ACT_FECHANACIMIENTO) values (4, NULL , 3, 'Salma Hayek', NULL, TO_DATE('02/09/1966', 'dd/mm/yyyy'));

insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (1, 1, 2000000, 120);

insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (1, 2, 3000000, 184);

insert into ACTUA (PEL_ID, ACT_ID, SALARIO, NUM_CONTRATO) values (2, 3, 1500000, 109);
