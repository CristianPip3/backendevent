<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtrToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function (Blueprint $table) {
           $table->boolean('is_admin')->default(false);
            $table->boolean('is_org')->default(false);
            $table->string('cedula')->nullable();
            $table->mediumText('descripcion_perfil')->nullable();
            $table->string('telefono')->nullable();
            $table->string('foto_perfil')->nullable();
            $table->string('tj_pro')->nullable();
            $table->string('cf_idm')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_admin');
            $table->dropColumn('is_org');
            $table->dropColumn('cedula');
            $table->dropColumn('descripcion_perfil');
            $table->dropColumn('telefono');
            $table->dropColumn('foto_perfil');
            $table->dropColumn('tj_pro');
            $table->dropColumn('cf_idm');
        });
    }
}
