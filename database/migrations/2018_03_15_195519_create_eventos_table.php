<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('id_guia')->unsigned();
            $table->foreign('id_guia')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->mediumText('descripcion');
            $table->string('fecha_inicio');
            $table->string('fecha_fin')->nullable();
            $table->string('precio')->nullable();
            $table->string('lugar_evento')->nullable();
            $table->string('imagen_evento')->nullable();
            $table->string('responsable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
