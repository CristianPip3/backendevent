<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register','Api\Auth\RegisterController@register');
Route::post('login', 'Api\Auth\LoginController@login');
Route::post('refresh','Api\Auth\LoginController@refresh');
Route::post('social_auth', 'Api\Auth\SocialAuthController@socialAuth');

/***********************************************/
Route::post('registerO','Api\Auth\RegisterOrganizadorController@registerO');
/*Routes eventos por categorias*/
Route::get('eventosAll', 'Api\PostController@eventosAll');
Route::get('eventosGastro', 'Api\PostController@eventosGastro');
Route::get('eventosSoci', 'Api\PostController@eventosSoci');
Route::get('eventosRecre', 'Api\PostController@eventosRecre');
Route::get('eventosReli', 'Api\PostController@eventosReli');
Route::get('eventosSalud', 'Api\PostController@eventosSalud');
Route::get('eventosAca', 'Api\PostController@eventosAca');




/*****************************/

Route::resource('user', 'UserController',
                ['only' => ['index', 'store', 'update', 'destroy', 'show']]);


Route::middleware('auth:api')->group(function() {
	Route::post('logout', 'Api\Auth\LoginController@logout');
	Route::post('store', 'Api\PostController@storeEvento');
	Route::post('storeComent', 'Api\PostController@storeComentario');

	Route::post('updateusr', 'Api\PostController@updateUser');
	Route::put('updateorg', 'Api\PostController@updateOrg');

	Route::get('comentarios', 'Api\PostController@comentarios');

	Route::get('profile', 'Api\PostController@profile');
	Route::get('posts', 'Api\PostController@index');
   // dd('Saludos');
    //return $request->user();
});

