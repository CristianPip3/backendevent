@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>organizadors CRUD</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('organizadors.create') }}"> Create New organizador</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th width="280px">Operation</th>
        </tr>
    @foreach ($organizadors as $organizador)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $organizador->name}}</td>
        <td>{{ $organizador->email}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('organizadors.show',$organizador->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('organizadors.edit',$organizador->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['organizadors.destroy', $organizador->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $organizadors->render() !!}
@endsection