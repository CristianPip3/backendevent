@extends('layouts.app')
@section('title','Detalles de usuario{{$user->id}}')
@section('content')
    <h4>Detalles de usuario{{ $user->id }}</h4>

     <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
           <label for="name">Nombre:</label>
           <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('name', $user->name) }}">
           <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         </div>
         <div class="form-group">
           <label for="email">Correo electrónico:</label>
           <input type="email" name = "email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email', $user->email) }}">
           <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         </div>
         <div class="form-group">
             <label for="exampleFormControlTextarea1">Example textarea</label>
             <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>

        
    </form>

    <p>
        <a href="{{ route('users.index') }}">Regresar al listado de usuarios</a>
    </p>
@endsection
   

