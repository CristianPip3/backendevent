@extends('layouts.app')

@section('title', "Crear usuario")

@section('content')
    <h4>Editar usuario</h4>

    @if ($errors->any())
        <div class="alert alert-danger">
            <h6>Por favor corrige los errores debajo:</h6>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
           <label for="name">Nombre:</label>
           <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('name', $user->name) }}">
           <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         </div>
         <div class="form-group">
           <label for="email">Correo electrónico:</label>
           <input type="email" name = "email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email', $user->email) }}">
           <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         </div>
         <div class="form-group">
           <label for="password">Contraseña:</label>
           <input type="password" name = "password" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Mayor a 6 caracteres">
           
         </div>

        <button type="submit">Actualizar usuario</button>
    </form>

    <p>
        <a href="{{ route('users.index') }}">Regresar al listado de usuarios</a>
    </p>
@endsection