<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>EventPopayan</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 20px;
            }
            .parrafo {
                font-size: 20px;

            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
         
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Iniciar Sesión</a>
                        <a href="{{ route('register') }}">Registrar</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <!--<div class="title m-b-md">
    
                </div>-->
                <div class="parrafo">
                    <img src="{{asset('imagenes/logoevent.jpg')}}" width="35%" height="35%" alt="Logo" >
                    <br>
                    <h5 align="justify">
                        EventPopayan te permitirá 
                    </h5>                        
                    <p align="justify" >

                        Publicar o ver los eventos que están por llevarse a cabo en la ciudad de popayán.

                        Encuentra eventos académicos, recreativos, sociales, religiosos, de salud y gastronómicos.  

                        Actualmente está en el proceso de pruebas.

                        Es el resultado de un emprendimiento iniciado en el SENA.  

                        En el siguiente link encontrarás una versión beta de la aplicación.  
                    </p>
                </div>

                <div class="links">
                    <a href="{{asset('imagenes/Event.sql')}}">Descarga</a>
                    <a href="https://github.com/CristianPip3/EventPopayan">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
