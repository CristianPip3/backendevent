<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'id_asistente', 'id_guia','descripcion','puntuacion',
    ];

    public function organizador(){
		return $this->belongsTo(Organizador::class, 'id_guia');
	}
	public function asistente(){
		return $this->belongsTo(User::class, 'id_asistente');
	}
}


