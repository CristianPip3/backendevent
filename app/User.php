<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use App\Post;
use App\Comentario;
use App\Evento;
use App\SocialAccount;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','cedula','genero', 'descripcion_perfil', 'telefono','is_org',
    ];
    protected $guarded = ['is_admin'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

 
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = ['is_admin' => 'boolean', 'is_org' => 'boolean'];

    

    public function socialAccounts(){
        return $this->hasMany(SocialAccount::class);
    }
    public function is_org(){
        return $this->is_org == true;
    }
    public function is_admin(){
        return $this->email == 'cristianbenavides@unicauca.edu.co';
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
    
    public function misEventos(){
        return $this->hasMany(Evento::class, 'id_asistente');
    }

    public function eventos(){
        return $this->hasMany(Evento::class, 'id_guia');
    }

    public function misComentarios(){
        return $this->hasMany(Comentario::class, 'id_asistente');
    }
    
    public function comentarios(){
        return $this->hasMany(Comentario::class, 'id_guia');
    }

    public function eventosPrincipal(){
        return Evento::all;
    }

    

}


