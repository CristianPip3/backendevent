<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{ 
	protected $fillable = [
        'id_guia', 'name', 'Categoria','descripcion','hora','fecha_inicio','fecha_fin','precio','lugar_evento','responsable',
    ];
    


	public function organizador(){
		return $this->belongsTo(User::class, 'id_guia');
	}

	 
	
}
