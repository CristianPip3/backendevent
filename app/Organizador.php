<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use App\Evento;
use App\Comentario;
use App\SocialAccount;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Organizador extends Model
{
    protected $fillable = [
        'name', 'email','password','cedula','genero', 'telefono', 'descripcion_perfil',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','cedula'
    ];
    public function socialAccounts(){
        return $this->hasMany(SocialAccount::class);
    }

    public function eventos(){
        return $this->hasMany(Evento::class, 'id_guia');
    }
    public function comentarios(){
        return $this->hasMany(Comentario::class, 'id_guia');
    }
}
