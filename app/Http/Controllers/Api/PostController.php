<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use App\Evento;
use App\User;
use App\Comentario;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
     public function index(){

    	//$posts = Auth::user()->posts()->get();
    	$eventos = Auth::user()->eventos()->get();


    	return response()->json(['data' => $eventos], 200, [],JSON_NUMERIC_CHECK);

    	//dd($posts);

    }
    public function comentarios(){

    	//$posts = Auth::user()->posts()->get();
    	$coment = Auth::user()->Comentarios()->get();

    	return response()->json(['data' => $coment], 200, [],JSON_NUMERIC_CHECK);

    	//dd($posts);

    }

    public function profile(){

    	$user  = Auth::user();

    	//dd($user);
    	return response()->json(['usuario' => $user], 200, [],JSON_NUMERIC_CHECK);

    }
    public function eventosAll(){
    	$eventosP = Evento::All();
    	/*$eventosG = Evento::where('Categoria','Gastronomia')->get();
    	$eventosS = Evento::where('Categoria','Social')->get();
    	$eventosR = Evento::where('Categoria','Recreativo')->get();
    	$eventosRe = Evento::where('Categoria','Religioso')->get();
    	$eventosSa = Evento::where('Categoria','Salud')->get();
    	$eventosA = Evento::where('Categoria','Academico')->get();*/
    	return response()->json(['data' => $eventosP/*,
    							'Gastronomia' => $eventosG,
    							'Social' => $eventosS,
    							'Recreativo' => $eventosR,
    							'Religioso' => $eventosRe,
    							'Salud' => $eventosSa ,
    							'Academico' => $eventosA*/], 200, [],JSON_NUMERIC_CHECK);

    }
    public function eventosGastro(){
    	$eventosG = Evento::where('Categoria','Gastronomia')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
     public function eventosSoci(){
    	$eventosG = Evento::where('Categoria','Social')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
     public function eventosRecre(){
    	$eventosG = Evento::where('Categoria','Recreativo')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
     public function eventosReli(){
    	$eventosG = Evento::where('Categoria','Religioso')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
     public function eventosSalud(){
    	$eventosG = Evento::where('Categoria','Salud')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
     public function eventosAca(){
    	$eventosG = Evento::where('Categoria','Academico')->get();
    	return response()->json(['data' => $eventosG], 200, [],JSON_NUMERIC_CHECK);

    }
    public function storeEvento(Request $request)    {
    	
    	Auth::user();
        $data = request()->validate([
            'id_guia' => 'required',
            'name' => 'required',
            'Categoria' => 'required',
            'descripcion' => 'required',
            'hora' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'precio' => 'required',
            'lugar_evento' => 'required',
            'responsable' => 'required',
        ]);
        //dd($data);
        Evento::create([
        	'id_guia' => $data['id_guia'],
            'name' => $data['name'],
            'Categoria' => $data['Categoria'],
            'descripcion' => $data['descripcion'],
            'hora' => $data['hora'],
            'fecha_inicio' => $data['fecha_inicio'],
            'fecha_fin' => $data['fecha_fin'],
            'precio' => $data['precio'],
            'lugar_evento' => $data['lugar_evento'],
            'responsable' => $data['responsable']
        ]);

		return response()->json(['data' => 'Registro Exitoso'], 200);
       
    }
    public function updateEvent(Request $request)
    {
        //dd($request);
        Auth::user();
        $data = request()->validate([
            'id_guia' => 'required',
            'name' => 'required',
            'Categoria' => 'required',
            'descripcion' => 'required',
            'hora' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'precio' => 'required',
            'lugar_evento' => 'required',
            'responsable' => 'required',
        ]);
        ;
        $user = Evento::find($data['id']);
        //($user);
        $user->update($data);
        
        return response()->json(['data' => 'Evento Actualizado'], 200, [],JSON_NUMERIC_CHECK);
    }
    public function storeComentario(Request $request){
        Auth::user();
        $data = request()->validate([
            'id_guia' => 'required',
            'id_asistente' => 'required',
            'descripcion' => 'required',
            'puntuacion' => 'required',
        ]);
         Comentario::create([
            'id_guia' => $data['id_guia'],
            'id_asistente' => $data['id_asistente'],
            'descripcion' => $data['descripcion'],
            'puntuacion' => $data['puntuacion'],
        ]);
        return response()->json(['data' => 'Registro Exitoso'], 200);

    }
    public function updateOrg(Request $request)
    { 
        Auth::user();

        $data = request()->validate([
            'id' => 'required',
            'name' => 'required',
            'cedula' => 'required',
            'descripcion_perfil' => 'required',
            'telefono' => 'required',
        ]);
        $user = User::find($data['id']);
        $user->is_org(true);
        $user->update($data);
        
        return response()->json(['data' => 'Actualizado'], 200, [],JSON_NUMERIC_CHECK);
    }
    public function updateUser(Request $request)
    {
        Auth::user();
        //dd($request);
        $data = request()->validate([
            'id' => 'required',
            'name' => 'required',
            'cedula' => 'required',
            'descripcion_perfil' => 'required',
            'telefono' => 'required',
        ]);
        ;
        $user = User::find($data['id']);
        //($user);
        $user->update($data);
        
        return response()->json(['data' => 'Actualizado'], 200, [],JSON_NUMERIC_CHECK);
    }

    public function deletEvento(Request $request ){
        Auth::user();
        $data = request()->validate([
            'id' => 'required',
        ]);
        $even = Evento::find($data['id']);
        $even->delete();
        return response()->json(['data' => 'Eliminado'], 200, [],JSON_NUMERIC_CHECK);
    }




}
