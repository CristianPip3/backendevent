<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use App\Organizador;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class RegisterOrganicadorController extends Controller
{
	use IssueTokenTrait;

	private $client;

	public function __construct(){
		$this->client = Client::find(2);
	}

    public function registerO(Request $request){

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:organizadors,email',
    		'password' => 'required|min:6'
    	]);

    	$organizador = Organizador::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => bcrypt(request('password'))
    	]);

    	return $this->issueToken($request, 'password');

    }
}
