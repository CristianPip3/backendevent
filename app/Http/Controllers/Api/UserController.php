<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use App\Evento;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $this->validate($request, [
            'name' => 'required',
            'cedula' => 'required',
            'descripcion_perfil' => 'required',
            'telefono' => 'redirect',
        ]);
        $user->is_org(true);
        $user->update($request->all());
        
        return response()->json(['data' => 'Actualizado'], 200, [],JSON_NUMERIC_CHECK);
    }


}